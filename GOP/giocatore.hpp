/*
 * casella.cpp
 *
 *  Created on: 23 gen 2018
 *      Author: Simone Grillini, Matteo Pampaloni & Emanuele Querzola
 */

#ifndef GIOCATORE_HPP_
#define GIOCATORE_HPP_

#include <string>

using namespace std;

class Giocatore {

	private:
		string nome;
		int posizione;
		int bloccato;

	public:
		Giocatore (string nome="");

		//setter e getter

		void setNome (string nome);

		//cambia = true sostituisce posizione con n, altrimenti lo somma
		// per decrementare inserire il numero negativo
		void setPosizione (int n=0, bool cambia=false);
		void setBloccato (int n=0, bool cambia=false);

		string getNome ();
		int getPosizione();
		int getBloccato();

};

#endif /* GIOCATORE_HPP_ */
