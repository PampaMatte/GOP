/*
 * casella.cpp
 *
 *  Created on: 23 gen 2018
 *      Author: Matteo Pampaloni, Emanuele Querzola, Simone Grillini
 */

#include <iostream>
#include <cstdlib>
#include <stdio.h>
#include <time.h>

#include "funzioniGioco.hpp"
#include "mazzo.hpp"
#include "interfaccia.hpp"

using namespace std;

ptr_lista Mazzo::getHead(){

	return head;
};

void Mazzo::stampa_mazzo(){
	int nCarta = 0; 
	ptr_lista tmp=Mazzo::getHead();
	while(tmp->next!=NULL){
		nCarta++;
		tmp=tmp->next;
		cout<< "  carta n." << nCarta << tmp->val << endl;
	}

}

ptr_lista Mazzo::crea_mazzo(){
	int lunghezza = gameSettings::nCarteMazzo;
	ptr_lista head, temp;
	head = NULL;
	srand (time(NULL));
		for(;lunghezza>0;lunghezza--){
			temp = new nodo;
			temp->val = (rand()%gameSettings::nEffettiCarte)+1 ;	//assegno ad ognin carta del mazzo(nodo della lista) un valore che va da 1 a nEffettiCarte che sono i tipi di effetti che possono avere le carte
			temp->next = head;
			head = temp;

		}

      Mazzo::head=head;
      //Mazzo::stampa_mazzo();  stampa del mazzo
		return head;
}

Mazzo::Mazzo() {
	posizione=0;
	head=crea_mazzo();

}

int Mazzo::getPosizione(){	//la posizione � una sortadi puntatore di tipo int alla carta che stiamo usando in modo che incrementandolo evitiamo di usare sempre la stessa carta

	return posizione;
}
void Mazzo::setPosizione(int n){

	this->posizione=n;
}

void Mazzo::aggiorna_pos(){	//incrementiamo la posizione
	int pos=getPosizione();
	if(pos==gameSettings::nCarteMazzo){	//controlliamo che la carta in gioco non sia l'ultima carta del mazzo
		setPosizione(0);//se lo � mettiamo pos a zero (simulando il gesto del mettere le carte in fondo al mazzo dopo averle giocate, quando arriveremo a giocare l'ultima carta
	}else{						//non ancora giocata avremo che la successiva carta risulter� essere la prima carta giocata, come un vettore circolare testa e caoda sono cos� virtualmente collegate)
		setPosizione(pos+1);
	}


}


void scambia_posizione(int numeroGiocatore1, int numeroGiocatore2, ptr_partecipanti ptr_lista){	//questa funzione serve per scambiare la posizione fra due giocatori, e viene richiamata in pi� situazioni

	int appoggio;
	ptr_partecipanti tmp=ptr_lista, tmp2=ptr_lista;
	for (appoggio=0;appoggio<numeroGiocatore1;appoggio++){	//cerco il primo giocatore da scambiare
		tmp=tmp->successivo;
	}
	int temporanea = tmp->giocatore.getPosizione();

	for (appoggio=0;appoggio<numeroGiocatore2;appoggio++){	//cerco il secondo giocatore da scambiare
				tmp2=tmp2->successivo;
			}
	tmp->giocatore.setPosizione(tmp2->giocatore.getPosizione(), true);
	tmp2->giocatore.setPosizione(temporanea, true);

}

	int Mazzo::pesca_carta(){	//ci permette di ritornare il valore (tipo carta) della carta puntata da posizione
		ptr_lista tmp=getHead();
		int pos=getPosizione();
		for(int i=0;i<pos;i++){
			tmp=tmp->next;
		}
		aggiorna_pos();
		return tmp->val;
	}

void Mazzo::effetto_usocarta(ptr_partecipanti ptr_lista, int nGiocatore){	//applichiamo tutti i possibili effetti dalle carte a seconda di quale viene pescata da pesca_caarta()
	int appoggio, appoggio2, primo, n;
	bool found = false;
	string altroGiocatore;
	ptr_partecipanti tmp=ptr_lista;
	for (appoggio=0;appoggio<nGiocatore;appoggio++){
		tmp=tmp->successivo;
	}
   cout << "\n  Effetto carta:\n";

	switch (pesca_carta()){ //richiamo della funzione pesca carta che restituisce il tipo della carta pescata che determina il suo effetto

		case 1:
			cout << "  Scambia la posizione con il giocatore in testa\n";
			primo=tmp->giocatore.getPosizione();
			tmp=ptr_lista;
			for (int appoggio=0; tmp != NULL;appoggio++){	//cerco il giocatore con la posizione pi� alta nel tabellone scorrendo la lista e usando una variabile d'appoggio per confrontare le posizioni dei giocatori
				if(tmp->giocatore.getPosizione() > primo)
					primo = tmp->giocatore.getPosizione();
				tmp=tmp->successivo;
			}
			for (int appoggio2=0; tmp != NULL;appoggio2++){
				if(tmp->giocatore.getPosizione() == primo){
					if (appoggio2 == nGiocatore){
						cout << "  complimenti sei gi� il giocatore pi� avanti\n";
					}else{
						scambia_posizione(nGiocatore, appoggio2, ptr_lista); // richiama la funzione scambio con la posizione nella lista dei due giocatori da scambiare
						}
					}
			}


			
		break;

		case 2:
			cout << "  Scambia la posizione con il giocatore pi� indietro\n";
			primo=tmp->giocatore.getPosizione();
			for (appoggio=0;tmp->successivo!=NULL;appoggio++){	//cerco il giocatore con la posizione pi� bassa nel tabellone scorrendo la lista e usando una variabile d'appoggio per confrontare le posizioni dei giocatori
					if(tmp->giocatore.getPosizione()<primo)
						primo=tmp->giocatore.getPosizione();
					tmp=tmp->successivo;
				}
					scambia_posizione(nGiocatore, appoggio, ptr_lista); // richiama la funzione scambio con la posizione nella lista dei due giocatori da scambiare
		break;

		case 3:
			cout << "Vai alla penultima casella ma stai fermo n turni\n";
			tmp->giocatore.setPosizione(gameSettings::nCaselleTab -1, true); 	//settare posizione = penultima casella
			tmp->giocatore.setBloccato(2, true);	//rimnere bloccato 2 turni
		break;

		case 4:
			cout << "  Torna all'inizio con un giocatore a tua scelta\n";
			cout << "  Nome altro giocatore -->\n";
			getline(cin, altroGiocatore);	// leggo il nome dell'altro giocatore
			tmp->giocatore.setPosizione(0, true);
			tmp=ptr_lista;
			do{
				for (appoggio=0;tmp->successivo!=NULL;appoggio++){	//scorro tutta la lista e cerco il giocatore col nome uguale a quello desiderato
					if(tmp->giocatore.getNome() == altroGiocatore){
						tmp->giocatore.setPosizione(0, true);	//e lo setto all'inizio del tabellone
						found = true;
					}
					tmp=tmp->successivo;
				}
				if(found == false)
					cout<<"nome giocatore inesistente, inserisci nuovamente il nome"<<endl;
			}while(found == false);
		break;

		case 5:
			srand (time(NULL));
			n = rand()%5;
			cout << "  Tira "<< n << " volte il dado e avanza\n";	//n � un numero rando che per convenzione mia va da 0 a 5 che definisce il numero di volte che il giocatore tirer� il dado per avanzare
			for(int i=0;i<n;i++){
				cout << "  Tiro n. "<< i+1 <<endl;
				appoggio= tiraDado();
				tmp->giocatore.setPosizione(tmp->giocatore.getPosizione()+appoggio); // setto la posizione del giocatore a : posizione precedente + risultato del tiro del dado
			}
		break;

		case 6:
			cout << "  i giocatori vengono riposizionati in modo random sul tabellone\n";
			tmp=ptr_lista;
			srand (time(NULL));
			while(tmp->successivo!=NULL){
				n = rand()%(gameSettings::nCaselleTab -1);	// creo un numero random minore o uguale al numero massimo di caselle nel tabellone - 1 in modo che non possa aver vinto con solo questa carta
				tmp->giocatore.setPosizione(n, true);	// setto la posizione del giocatore al numero appena creato
				tmp=tmp->successivo;
			}
		break;

		case 7:
			cout << "  scambia due avversari\n";
			do{
				cout << "  nome primo giocatore ->\n";
				getline(cin, altroGiocatore);
				tmp=ptr_lista;
				for (appoggio=0;(tmp->giocatore.getNome())!= altroGiocatore;appoggio++){ //ciclo finch� il nome de giocatore che sto scorrendo nella lista non coincide con quello richiesto, ottengo cos� la posizione del giocatore in appoggio
					if(tmp->giocatore.getNome() == altroGiocatore)
						found = true;
					tmp=tmp->successivo;
				}
				if(found == false)
					cout<<"nome giocatore inesistente, inserisci nuovamente il nome"<<endl;
			}while(found == false);
			do{
				cout << "  nome secondo giocatore ->\n";
				getline(cin, altroGiocatore);
				tmp=ptr_lista;
				for (appoggio2=0;tmp->giocatore.getNome() != altroGiocatore;appoggio2++){
					if(tmp->giocatore.getNome() == altroGiocatore)
						found = true;
					tmp=tmp->successivo;
				}
					if(found == false)
						cout<<"nome giocatore inesistente, inserisci nuovamente il nome"<<endl;
			}while(found == false);
			scambia_posizione(appoggio, appoggio2, ptr_lista); // richiamo la funzione scambiando la posizione dei giocatori, gli passo la posizione dei giocatori nella lista per poterli trovare nella funzione con un ciclo

		break;

		case 8:
			srand (time(NULL));
			n = rand() % 5;
			cout << "  tira "<< n << " volte il dado e vai indietro\n";	//n � un numero rando che per convenzione mia va da 0 a 5 che definisce il numero di volte che il giocatore tirer� il dado per avanzare
			for(int i=0;i<n;i++){
				appoggio=tiraDado();
				tmp->giocatore.setPosizione(tmp->giocatore.getPosizione()-appoggio, true); // setto la posizione del giocatore a : posizione precedente + risultato del tiro dei dadi
			}
		break;

		default:
			cout << "  nessuna sorpresa per te questa volta\n";

		break;


	}

}

