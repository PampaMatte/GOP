/*
 * casella.cpp
 *
 *  Created on: 23 gen 2018
 *      Author: Simone Grillini, Matteo Pampaloni & Emanuele Querzola
 */

#include <ctime>
#include <iostream>
#include <cstdlib>
#include "funzioniGioco.hpp"
#include "interfaccia.hpp"
#include "giocatore.hpp"
#include "casella.hpp"
#include "mazzo.hpp"

using namespace std;

namespace gameSettings {

	//Effetti
	const int nEffettiCaselle = 7;	//  L'effetto nullo non si conta (0)
	const int nEffettiCarte = 13;	//  L'effetto nullo non si conta (0)

	//Dado
	bool dadoAutomatico = true;
	int minValDado = 1;
	int maxValDado = 6;

	//Giocatori
	const int minNumGiocatori = 2;
	const int maxNumGiocatori = 6;
	int nGiocatori=0;

	//Tabellone
	int minCaselleTab = 18 * 3 + 4;  //  58
	int maxCaselleTab = 200;
	int nCaselleTab;

	//Mazzo
	const int minCarteMazzo = 40;
	const int maxCarteMazzo = 80;
	int nCarteMazzo = 40;

	void setDadoAutomatico (bool b){
		dadoAutomatico = b;
	}

	void setMinValDado (){
		gestioneTerminale::clear();
		int n=11;
		//ciclo che continua finch l'utente non inserisce un parametro corretto
		while (n<0 || n>10 || n>maxValDado){
			cout << "  Inserisci un valore tra 0 e 10: ";
			cin>>n;
			gestioneTerminale::clear();
			//controlla che il numero vada bene
			if (n>=0 && n<11 && n<=maxValDado)
				minValDado=n;
			else //altrimenti stampa errore
				cout << "  Valore errato\n";
		}
	}

	void setMaxValDado (){
		gestioneTerminale::clear();
		int n=5;
		while (n<6 || n>minCaselleTab || n<minValDado){
			cout << "  Inserisci un valore maggiore o uguale a 6: ";
			cin>>n;
			gestioneTerminale::clear();
			if (n>=6 && n<=minCaselleTab && n>=minValDado)
				maxValDado=n;
			else
				cout << "  Valore errato\n";
		}
	}

	void setNGiocatori (int n){
		nGiocatori=n;
	}

	void setMinCaselleTab (){
		gestioneTerminale::clear();
		int n=0;
		while (n < 58 || n >= maxCaselleTab){
			cout << "  Inserisci un valore superiore o uguale a 58: ";
			cin >> n;
			gestioneTerminale::clear();
			if (n >= 58 && n < maxCaselleTab)
				minCaselleTab=n;
			else
				cout << "  Valore errato\n";
		}
	}

	void setMaxCaselleTab (){
		gestioneTerminale::clear();
		int n=0;
		while (n < 58 || n <= minCaselleTab){
			cout << "  Inserisci un valore superiore o uguale a 58: ";
			cin>>n;
			gestioneTerminale::clear();
			if (n >= 58 && n > minCaselleTab)
				maxCaselleTab=n;
			else
				cout << "  Valore errato\n";
		}
	}

	void setNCaselleTab (int n){
		if (n>=minCaselleTab && n<=maxCaselleTab)
			nCaselleTab=n;
	}

	void setNCarteMazzo (){
		gestioneTerminale::clear();
		int n=0;
		while (n<minCarteMazzo || n>maxCarteMazzo){
			cout << "  inseriusci un valore compreso tra " << minCarteMazzo << " e " << maxCarteMazzo << ": ";
			cin>>n;
			gestioneTerminale::clear();
			if (n>=minCarteMazzo && n<=maxCarteMazzo)
				nCarteMazzo=n;
			else
				cout << "  Valore errato\n";
		}
	}
}

int random( int max,int min ) { return rand() % ( max - min ) + min; }

int tiraDado(){
	int n;
	if (gameSettings::dadoAutomatico){
		n = random(gameSettings::maxValDado,gameSettings::minValDado+1);
		cout << "  Tiro del dado...\n  Valore: " << n << endl;
		//gestioneTerminale::pause( "  Premere un tasto per continuare" );
	}else{
		n = gameSettings::minValDado-1;
		while (n<gameSettings::minValDado || n>gameSettings::maxValDado){
			cout <<"  Tira il dado: ";
			cin>>n;
			if (n<gameSettings::minValDado || n>gameSettings::maxValDado)
				cout << "  Il valore deve essere compreso tra " << gameSettings::minValDado << "e " << gameSettings::maxValDado;
		}
	}

	return n;
}

void usoEffettoCasella( ptr_partecipanti ptr_lista, int nGiocatore, Casella casella, Mazzo mazzo ){
	int appoggio;
	ptr_partecipanti tmp=ptr_lista;
	for (appoggio=0;appoggio<nGiocatore;appoggio++){
		tmp=tmp->successivo;
	}

	switch (casella.getTipo()){
	case 1:
		//effetto pesca una carta
		mazzo.effetto_usocarta(ptr_lista, nGiocatore);
		break;
	case 2:
		//effetto tira il dado e blocca il giocatore
		cout << "\n  Effetto casella: Tira il dado e stai fermo il numero di turni che esce\n";
		appoggio=tiraDado();
		tmp->giocatore.setBloccato(appoggio);
		break;
	case 3:
		//effetto avanza in modo casuale
		appoggio=gameSettings::nCaselleTab-tmp->giocatore.getPosizione();
		appoggio = random(appoggio);
		tmp->giocatore.setPosizione(appoggio);
		cout << "\n  Effetto casella: Avanza di " << appoggio << " caselle.\n";
		break;
	case 4:
		//effetto indietreggia in modo casuale
		appoggio = random(tmp->giocatore.getPosizione());
		tmp->giocatore.setPosizione(-appoggio);
		cout << "\n  Effetto casella: Indietreggia di " << appoggio << " caselle.\n";
		break;
	case 5:
		//effetto ritira il dado e avanza
		cout << "\n  Effetto casella: Tira il dado e avanza\n";
		appoggio=tiraDado();
		tmp->giocatore.setPosizione(appoggio);
		break;
	case 6:
		//effetto ritira il dado e indietreggia
		cout << "\n  Effetto casella: Tira il dado e indietreggia\n";
		appoggio=-tiraDado();
		tmp->giocatore.setPosizione(appoggio);
		break;
	case 7:
		//effetto tira il dado se il numero  pari avanza del numero se  dispari rimani bloccato il numero di volte uscito
		cout << "\n  Effetto casella: Tira il dado, se il numero  pari avanzi, altrimenti rimani bloccato il numero di turni che esce\n";
		appoggio=tiraDado();
		if (appoggio%2==0)
			tmp->giocatore.setPosizione(appoggio);
		else
			tmp->giocatore.setBloccato(appoggio);
		break;
	}
	//if (casella.getTipo() != 0) 
		gestioneTerminale::pause( "  Premere un tasto per continuare" );
}

bool vittoriaGiocatore ( Giocatore giocatore )
{
	return ( gameSettings::nCaselleTab == giocatore.getPosizione() );
}
