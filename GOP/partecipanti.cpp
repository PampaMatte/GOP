/*
 * casella.cpp
 *
 *  Created on: 23 gen 2018
 *      Author: Simone Grillini, Matteo Pampaloni & Emanuele Querzola
 */


#include "partecipanti.hpp"
#include "giocatore.hpp"
#include <string>
#include <iostream>

using namespace std;

Partecipanti::Partecipanti (string nome){

	this->giocatore = Giocatore(nome);
	this->successivo = NULL;
}

ptr_partecipanti aggiungiPartecipante(string nome,ptr_partecipanti ptr_lista){
	//controlla che la lista non sia vuota, altrimenti la crea
	if (ptr_lista==NULL){
		ptr_lista = new Partecipanti(nome);
		return ptr_lista;
	}else{
		ptr_partecipanti tmp = ptr_lista;
		while (tmp->successivo!=NULL){
			tmp=tmp->successivo;
		}
		tmp->successivo = new Partecipanti(nome);
		return ptr_lista;
	}
}

Giocatore *getPartecipante (ptr_partecipanti ptr_lista, int n){

	for (int i=0;i<n && ptr_lista!=NULL;i++)
		ptr_lista=ptr_lista->successivo;

	if (ptr_lista==NULL)
		return NULL;
	else
		return &ptr_lista->giocatore; //ritorna l'indirizzo dell'oggetto

}

void setPartecipante (ptr_partecipanti ptr_lista, Giocatore giocatore, int n){

	for (int i=0;i<n && ptr_lista!=NULL;i++)
			ptr_lista=ptr_lista->successivo;

	if (ptr_lista!=NULL)
		ptr_lista->giocatore=giocatore;


}

ptr_partecipanti eliminaPartecipante(ptr_partecipanti ptr_lista,string nome){
	//controllo che la lista non sia vuota
	if (ptr_lista==NULL)
		return NULL;
	//controllo se � il primo nodo quello da eliminare
	if (ptr_lista->giocatore.getNome()==nome)
		return ptr_lista->successivo;

	ptr_partecipanti tmp = ptr_lista;

	//cerco il nodo della lista da eliminare
	while (tmp->successivo!=NULL || tmp->successivo->giocatore.getNome()==nome){
		if (tmp->successivo->giocatore.getNome()!=nome)
			tmp=tmp->successivo;
	}
	//elimino il nodo se � stato trovato
	if (tmp->successivo!=NULL)
		tmp->successivo = tmp->successivo->successivo;

	return ptr_lista;
}
