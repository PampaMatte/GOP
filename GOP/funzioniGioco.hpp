/*
 * casella.cpp
 *
 *  Created on: 23 gen 2018
 *      Author: Simone Grillini, Matteo Pampaloni & Emanuele Querzola
 */

#ifndef FUNZIONIGIOCO_HPP_
#define FUNZIONIGIOCO_HPP_

#include "giocatore.hpp"
#include "casella.hpp"
//#include "interfaccia.hpp"
#include "partecipanti.hpp"
#include "mazzo.hpp"

//variabili globali e funzioni utili per la creazione e lo svolgimento della partita
namespace gameSettings {

	//Effetti
	extern const int nEffettiCaselle;
	extern const int nEffettiCarte;

	//Dado
	extern bool dadoAutomatico;
	extern int minValDado;
	extern int maxValDado;

	//Giocatori
	extern const int minNumGiocatori;
	extern const int maxNumGiocatori;
	extern int nGiocatori;

	//Tabellone
	extern int minCaselleTab;
	extern int maxCaselleTab;
	extern int nCaselleTab;

	//Mazzo
	extern const int minCarteMazzo;
	extern const int maxCarteMazzo;
	extern int nCarteMazzo;

	//mettere?
	//extern int difficoltà;

	//getter e setter

	void setDadoAutomatico (bool b);
	void setMinValDado ();
	void setMaxValDado ();
	void setNGiocatori (int n);
	void setMinCaselleTab ();
	void setMaxCaselleTab ();
	void setNCaselleTab (int n);
	void setNCarteMazzo ();

}

//funzione che ritorna un numero rand compreso tra i parametri inseriti
int random(int max,int min=0);

//funzioni dado
int tiraDado();

//attiva gli effetti della casella
void usoEffettoCasella(ptr_partecipanti ptr_lista,int nGiocatore,Casella casella, Mazzo mazzo);

//controllo se il giocatore ha vinto
bool vittoriaGiocatore (Giocatore giocatore);

#endif /* FUNZIONIGIOCO_HPP_ */
