/*
 * casella.cpp
 *
 *  Created on: 23 gen 2018
 *      Author: Simone Grillini, Matteo Pampaloni & Emanuele Querzola
 */

#include "casella.hpp"
#include "tabellone.hpp"

#include <iostream>
#include <stdlib.h>

ptr_tabellone creaTabellone(){

	ptr_tabellone tabellone = new Tabellone, tmp=tabellone;

	int dimensioneTab = random(gameSettings::maxCaselleTab,gameSettings::minCaselleTab);
	gameSettings::setNCaselleTab(dimensioneTab); //salva la dimensione della tabella

	//crea le caselle
	for (int i=0;i<=dimensioneTab;i++){
		tmp->casella.setCasella(i);
		if (dimensioneTab==i)
			tmp->successivo = NULL;
		else{
			tmp->successivo = new Tabellone;
			tmp = tmp->successivo;
		}
		//Sleep(10);
	}
	return tabellone;
}

Casella getCasellaDaTab( ptr_tabellone tabellone, int n ) {
	//ptr_tabellone tmp = tabellone;
	for( int i = 0; i < n; i++ )
		tabellone = tabellone->successivo;
	return tabellone->casella;
}

Casella getCasella( ptr_tabellone tabellone ) { return tabellone->casella; }
