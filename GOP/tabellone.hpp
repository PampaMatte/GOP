/*
 * casella.cpp
 *
 *  Created on: 23 gen 2018
 *      Author: Simone Grillini, Matteo Pampaloni & Emanuele Querzola
 */

#ifndef TABELLONE_HPP_
#define TABELLONE_HPP_

#include "casella.hpp"
#include "funzioniGioco.hpp"

struct Tabellone {
	Casella casella;
	Tabellone *successivo;
};

typedef Tabellone *ptr_tabellone;

ptr_tabellone creaTabellone ();

Casella getCasellaDaTab (ptr_tabellone tabellone, int n);

Casella getCasella( ptr_tabellone );

#endif /* TABELLONE_HPP_ */
