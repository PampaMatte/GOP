/*
 * casella.cpp
 *
 *  Created on: 23 gen 2018
 *      Author: Simone Grillini, Matteo Pampaloni & Emanuele Querzola
 */

#ifndef INTERFACCIA_HPP_
#define INTERFACCIA_HPP_

#include "partecipanti.hpp"
#include "tabellone.hpp"
#define SEGMENTO 20

namespace gestioneTerminale {

	extern void pause( string message );	

	extern void clear();

	extern void wait( unsigned int seconds );

}


//funzione che crea l'interfaccia per settare le impostazioni della partita.
void menuSettings ();

//funzione che crea l'interfaccia del menu principale
void menu ();

//funzione in cui viene gestita la partita
void partita ();

void stampaMargine( int num_spazi, string carattere );

void aggiornaMatrice( string matrix[][SEGMENTO], ptr_partecipanti prt_lista );

char getCarattere( int posizioni_giocatori[], int casella, Casella casella_attuale );

void stampaMatrice( char matrix[][SEGMENTO] );

void stampaCarattere( char carattere );

void stampaGioco( ptr_partecipanti ptr_lista, ptr_tabellone tabellone );

void pause( string message );

#endif /* INTERFACCIA_HPP_ */
