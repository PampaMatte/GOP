/*
 * casella.cpp
 *
 *  Created on: 23 gen 2018
 *      Author: Simone Grillini, Matteo Pampaloni & Emanuele Querzola
 */

#ifndef CASELLA_HPP_
#define CASELLA_HPP_

class Casella {

	private:
		int posizione;
		int tipo;

	public:
		void setCasella(int pos);
		int getTipo ();
		int getPosizione();
};

typedef Casella *ptr_casella;

#endif /* CASELLA_HPP_ */
