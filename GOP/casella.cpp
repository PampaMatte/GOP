/*
 * casella.cpp
 *
 *  Created on: 23 gen 2018
 *      Author: Simone Grillini, Matteo Pampaloni & Emanuele Querzola
 */

#include "funzioniGioco.hpp"
#include "casella.hpp"

using namespace std;

void Casella::setCasella(int pos){
   int n=rand()%10;
	posizione=pos;
	if (pos==0 || pos==gameSettings::nCaselleTab)
		tipo=0;
	else{
         if(n%2==0)
            tipo =  (rand()%(gameSettings::nEffettiCaselle-1))+1; //random(gameSettings::nEffettiCaselle); //setta in modo random il tipo di casella
         else
            tipo =0;
	}
	//cout<< "posizione = "<< posizione << "  tipo effetto casella = "<< tipo <<endl;  //controllo creazione caselle e valori pseudo random
}

int Casella::getTipo (){
	return tipo;
}
int Casella::getPosizione(){
	return posizione;
}

