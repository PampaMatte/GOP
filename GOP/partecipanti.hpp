/*
 * casella.cpp
 *
 *  Created on: 23 gen 2018
 *      Author: Simone Grillini, Matteo Pampaloni & Emanuele Querzola
 */

#ifndef PARTECIPANTI_HPP_
#define PARTECIPANTI_HPP_

#include "giocatore.hpp"
#include <string>

using namespace std;

struct Partecipanti {

	Giocatore giocatore;
	Partecipanti *successivo;

	Partecipanti (string nome);
};

typedef Partecipanti *ptr_partecipanti;

ptr_partecipanti aggiungiPartecipante(string nome, ptr_partecipanti ptr_lista);

//dato un numero n restituisce l'oggetto situato in quella posizione altrimenti NULL
Giocatore *getPartecipante (ptr_partecipanti ptr_lista,int n);

//modifica il partecipante nella data posizione
void setPartecipante(ptr_partecipanti ptr_lista, Giocatore giocatore, int n);

//elimina un partecipante
ptr_partecipanti eliminaPartecipante(ptr_partecipanti ptr_lista,string nome);


#endif /* PARTECIPANTI_HPP_ */
