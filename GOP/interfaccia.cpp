/*
 * casella.cpp
 *
 *  Created on: 23 gen 2018
 *      Author: Simone Grillini, Matteo Pampaloni & Emanuele Querzola
 */

//  Guardia per il refresh del terminale
#ifdef __cplusplus__
  #include <cstdlib>
#else
  #include <stdlib.h>
#endif

#include <iostream>
#include <ctime>
#include "interfaccia.hpp"
#include "funzioniGioco.hpp"
#include "partecipanti.hpp"
#include "tabellone.hpp"
#include "mazzo.hpp"
#define MARGINE_SINISTRO 11
#define SEGMENTO 20

namespace gestioneTerminale {

	void pause( string message )
	{
		cout << "\n" << message << " ... ";
		getchar();
	}	

	void clear()
	{ 
		//if ( system( "CLS" ) ) system( "clear" );
		for( int i = 0; i < 500; i++ ) cout << "\n";
	}

	void wait( unsigned int seconds )
	{
		unsigned int finish_time = time( 0 ) + seconds;
    	while ( time( 0 ) < finish_time );
	}

}

void menuSettings ()
{
	int azione = 1;
	gestioneTerminale::clear();
	//continuo finchè l'utente non preme 0 per uscire
	while ( azione != 0 )
	{
		// stampo i parametri delle impostazioni
		cout << "  Impostazioni\n\n";

		if ( gameSettings::dadoAutomatico )
			cout << "  1. Dado: Automatico\n";
		else
			cout << "  1. Dado: Manuale\n";

		cout << "  2. Valore minimo del dado: " << gameSettings::minValDado << endl;
		cout << "  3. Valore massimo del dado: " << gameSettings::maxValDado << endl;
		cout << "  4. Numero minimo di caselle: " << gameSettings::minCaselleTab << endl;
		cout << "  5. Numero massimo di caselle: " << gameSettings::maxCaselleTab << endl;
		cout << "  6. Numero di carte del mazzo: " << gameSettings::nCarteMazzo << endl;
		cout << "\n  Inserisci il valore dell'impostazione da modificare (0 per uscire): ";
		cin >> azione;

		gestioneTerminale::clear(); //system("clear");
		//eseguo l'azione richiesta dall'utente
		switch ( azione )
		{
			case 1:
				if ( gameSettings::dadoAutomatico )
					gameSettings::setDadoAutomatico( false );
				else
					gameSettings::setDadoAutomatico( true );
				break;
			case 2:
					gameSettings::setMinValDado();
				break;
			case 3:
				gameSettings::setMaxValDado();
				break;
			case 4:
				gameSettings::setMinCaselleTab();
				break;
			case 5:
				gameSettings::setMaxCaselleTab();
				break;
			case 6:
				gameSettings::setNCarteMazzo();
				break;
			default:
				if ( azione != 0 )
					cout << "  Valore inesistente\n";
		}
	}
}

void print_logo( int foreground ) {
  cout << "\n\n\033[" << foreground << "m";
  cout << "                   ╔═══════════╗ ╔═══════════╗ ╔═══════════╗ \n";
  cout << "                   ║           ║ ║           ║ ║           ║ \n";
  cout << "                   ║  ╔════════╝ ║  ╔═════╗  ║ ║  ╔═════╗  ║ \n";
  cout << "                   ║  ║  ╔═════╗ ║  ║     ║  ║ ║  ║     ║  ║ \n";
  cout << "                   ║  ║  ║     ║ ║  ║     ║  ║ ║  ╚═════╝  ║ \n";
  cout << "                   ║  ║  ╚══╗  ║ ║  ║     ║  ║ ║  ╔════════╝ \n";
  cout << "                   ║  ╚═════╝  ║ ║  ╚═════╝  ║ ║  ║          \n";
  cout << "                   ║           ║ ║           ║ ║  ║          \n";
  cout << "                   ╚═══════════╝ ╚═══════════╝ ╚══╝          \033[0m\n\n\n";
}

void menu (){

	//  Schermata di benvenuto
	print_logo( 34 );
    //gestioneTerminale::pause( "Premere un tasto per continuare" );

	//  Menu
	//gestioneTerminale::clear(); //system("clear");
	int n = 0;
	cout << "  \033[1;33mBENVENUTO!\033[0m\n\n" << endl;

	while ( n != 3 )
	{
		cout << "  1. Impostazioni\n  2. Gioca\n  3. Esci\n";
		cout << "\n  Scegli il numero dell'azione da compiere: ";
		cin >> n;

		switch ( n )
		{
			case 1:
				menuSettings();
				break;
			case 2:
				partita();
				break;
			default:
				if ( n != 3 ) // Controllo che non sia numero per uscire (3)
					cout << "  Errore: Valore inesistente nel menu'\n";
				break;
		}
	}
	cout << "  Arrivederci!";
}

//  Funzione che gestisce la partita
void partita() {
    gestioneTerminale::clear();
	//  Creazione delle liste
	//  Creazione della lista dei partecipanti
	ptr_partecipanti partecipanti = NULL;
	string nomeGiocatore = "g1";
//	while ( ( nomeGiocatore != "-1" || gameSettings::nGiocatori < gameSettings::minNumGiocatori ) && gameSettings::nGiocatori < gameSettings::maxNumGiocatori )  //  Ciclo per inserire i giocatori
//	{	
		//int tentativo = 0;
		bool trovato = false;
		do{	
			if( trovato )
			{
				cout<<"  Nome giocatore già utilizzato, inserisci una variante non utilizzata\n";
				trovato = false;
			}
			
			cout << "  Inserisci il nome del giocatore (inserisci -1 per finire): ";
			cin >> nomeGiocatore;
			ptr_partecipanti temp = partecipanti;
			while ( temp != NULL && !trovato ) {
				nomeGiocatore == temp->giocatore.getNome() ? trovato = true : trovato = false;
				//cout << "  [" << temp->giocatore.getNome() << "]\n";
				temp = temp->successivo;
			}

			//gestioneTerminale::clear(); //system ("clear");
			if( !trovato ) 
			{
				if ( nomeGiocatore == "-1" && gameSettings::nGiocatori < gameSettings::minNumGiocatori )
				{
					cout << "  Errore: numero minimo di giocatori: " << gameSettings::minNumGiocatori << ".\n";
				}
				else if( nomeGiocatore != "-1" )  //  Controla se il nome è corretto e in caso positivo aggiunge il giocatore
				{ 
					if ( partecipanti == NULL )
						partecipanti = new Partecipanti ( nomeGiocatore );
					else
						partecipanti = aggiungiPartecipante( nomeGiocatore, partecipanti );
					gameSettings::nGiocatori++;
					trovato = false;
				}
			}
		} while( ( nomeGiocatore != "-1" && gameSettings::nGiocatori < gameSettings::maxNumGiocatori ) || gameSettings::nGiocatori < gameSettings::minNumGiocatori );
//	}

	//  Creazione del tabellone
	ptr_tabellone tabellone = creaTabellone();
	//  Creazione del mazzo
	Mazzo mazzo = Mazzo();

    //  Gestione della partita
	int n;  //  Variabile d'appoggio
	ptr_partecipanti partecipanteDiTurno = partecipanti;  //  Puntatore che indica il giocatore che deve svolgere il turno
	Casella casella;  //  Variabile d'appoggio per l'oggetto casella
	bool finito = false;  //  Se true vuol dire che qualcuno ha vinto

	//messaggi all'utente
	gestioneTerminale::wait( 1 );
	cout << "  Il caricamento e' completo\n\n";
	//stampaGioco( partecipanti,tabellone ); //stampo il tabellone
	cout << endl;
 
	gestioneTerminale::pause( "  Premere un tasto per continuare" );
	gestioneTerminale::clear();
	
	stampaGioco( partecipanti,tabellone );

	while ( !finito )
	{
		for ( int i = 0; i < gameSettings::nGiocatori && !finito; i++ )  //  Ciclo che gestisce i turni
		{ 
			if ( partecipanteDiTurno->giocatore.getBloccato() == 0 )
			{
			    cout << "  Turno di " << partecipanteDiTurno->giocatore.getNome() << endl;
				n = tiraDado();
				partecipanteDiTurno->giocatore.setPosizione( n );  //  Faccio avanzare il giocatore
				n = partecipanteDiTurno->giocatore.getPosizione();
				casella = getCasellaDaTab( tabellone, n );
				usoEffettoCasella( partecipanti, i, casella, mazzo );  //  Attivo l'effetto della casella
				finito = vittoriaGiocatore( partecipanteDiTurno->giocatore );

				gestioneTerminale::clear();
				//  Funzione di stampa
				stampaGioco( partecipanti,tabellone );
			}
			else
				partecipanteDiTurno->giocatore.setBloccato(-1);  //  Decremento il numero di turni in cui il giocatore è bloccato			

			if ( !finito )   //  Controllo che non sia finita la partita e passo al turno del giocatore successivo
			{
				if ( i == gameSettings::nGiocatori - 1 )
					partecipanteDiTurno = partecipanti;
				else
					partecipanteDiTurno = partecipanteDiTurno->successivo;
			}else{
				gestioneTerminale::clear();
				cout<<"  Complimenti! " + partecipanteDiTurno->giocatore.getNome() + " ha vinto la partita!";
				gestioneTerminale::wait( 3 );
				gestioneTerminale::clear();
			}
		}
	}

	//eliminazione delle liste
	//eliminazione la lista dei partecipanti
	partecipanteDiTurno = partecipanti->successivo;
	delete partecipanti;
	while ( partecipanteDiTurno->successivo != NULL )  //  Ciclo per eliminare i nodi
	{
		partecipanti = partecipanteDiTurno;
		partecipanteDiTurno=partecipanteDiTurno->successivo;
		delete partecipanti;
	}
	delete partecipanteDiTurno; //  Elimina l'ultimo nodo
	gameSettings::nGiocatori = 0; //  Riazzero la variabile nGiocatori

	//eliminazione il tabellone
	ptr_tabellone tmp1 = tabellone->successivo;
	delete tabellone;
	while ( tmp1->successivo != NULL )
	{
		tabellone = tmp1;
		tmp1 = tmp1->successivo;
		delete tabellone;
	}
	delete tmp1;
}

void stampaMargine( int num_spazi, string carattere )
{
	for ( int x = 0; x < num_spazi; x++ ) cout << carattere;
}

/**/
void aggiornaMatrice( char matrix[][SEGMENTO], ptr_partecipanti prt_lista, ptr_tabellone tabellone )
{
	int y;
	( ( gameSettings::nCaselleTab ) % SEGMENTO ) > 0 ? y = ( ( gameSettings::nCaselleTab ) / SEGMENTO ) + 1 : y = ( gameSettings::nCaselleTab ) / SEGMENTO;

	ptr_partecipanti giocatori = prt_lista;
	ptr_tabellone percorso = tabellone;

	int posizioni_giocatori[gameSettings::nGiocatori];	//  Memorizzo nell'array le posizioni dei giocatori
	int indice = 0;
	while ( giocatori != NULL && indice < gameSettings::nGiocatori )
	{
		Giocatore tmp = giocatori->giocatore;
		posizioni_giocatori[indice] = tmp.getPosizione();
		giocatori = giocatori->successivo;
		indice++;
	}

	int casella = 0;

	for ( int riga = 0; riga < y; riga++ )
	{
		for ( int colonna = 0; colonna < SEGMENTO && casella <= gameSettings::nCaselleTab ; colonna++ )
		{	
			Casella casella_attuale = getCasella( percorso );
			matrix[riga][colonna] = getCarattere( posizioni_giocatori, casella, casella_attuale );
			percorso = percorso->successivo;
			casella++;		
		}
	}
}

char getCarattere( int posizioni_giocatori[], int casella, Casella casella_attuale )
{
	char char_giocatore = 'A';
	char risultato; 

	casella_attuale.getTipo() == 0 ? risultato = '*' : risultato = '?';
		
	for ( int i = 0; i < gameSettings::nGiocatori; i++ )	//  Scorro i giocatori
	{
		if ( posizioni_giocatori[i] == casella )
			risultato = ( char_giocatore + i );
	}

	return risultato;
}

void stampaMatrice( char matrix[][SEGMENTO] )
{
	int y, num_elementi = 18;	//  Elementi standard della riga
	( ( gameSettings::nCaselleTab ) % SEGMENTO ) > 0 ? y = ( ( gameSettings::nCaselleTab ) / SEGMENTO ) + 1 : y = ( gameSettings::nCaselleTab ) / SEGMENTO;

	int casella = 0;	//  Numero
	bool destra = true, inverso = false;

	stampaMargine( MARGINE_SINISTRO, " " );
	cout << "╔"; stampaMargine( ( num_elementi * 3 ) + 2, "═" ); cout << "╗\n";

	for ( int riga = 0; riga < y; riga++ )
	{
		if ( !inverso )	//  Stampa dritta 
		{
			stampaMargine( MARGINE_SINISTRO, " " ); cout << "║ ";
			for ( int colonna = 0; colonna < num_elementi && casella < gameSettings::nCaselleTab; colonna++, casella++ )
			{	
				cout << " ";
				stampaCarattere( matrix[riga][colonna] );
				cout << " ";
			}

			cout << " ║\n";

			if ( casella < gameSettings::nCaselleTab )
			{
				// INTERLINEA 1
				stampaMargine( MARGINE_SINISTRO, " " );
				cout << "╚";
				stampaMargine( ( num_elementi - 1 ) * 3 - 1, "═" );
				cout << "╗  ";
				stampaCarattere( matrix[riga][num_elementi] );
				cout << "  ║\n";
				casella++;
			}

			int rimanente = gameSettings::nCaselleTab - casella - 2;

			if ( casella < gameSettings::nCaselleTab )
			{
				// INTERLINEA 2
				if ( rimanente >= num_elementi )
				{
					stampaMargine( MARGINE_SINISTRO, " " );
					cout << "╔";
					stampaMargine( ( num_elementi - 1 ) * 3 - 1, "═" );
					cout << "╝  ";
					stampaCarattere( matrix[riga][num_elementi + 1] );
					cout << "  ║\n";
					casella++;
				}
				else
				{
					if ( rimanente <= 0 )
					{
						stampaMargine( MARGINE_SINISTRO + ( ( num_elementi - 1 ) * 3 + 1 ), " " );
						cout << "║ ";
						stampaCarattere( matrix[riga][num_elementi + 1] );
						cout << "  ║\n";
						casella++;
					}
					else
					{
						stampaMargine( MARGINE_SINISTRO + ( ( num_elementi - rimanente ) - 1 ) * 3, " " );
						cout << "╔";
						stampaMargine( rimanente * 3 - 1, "═" );
						cout << "╝  ";
						stampaCarattere( matrix[riga][num_elementi + 1] );
						cout << "  ║\n";
						casella++;
					}
					
				}
			}

			inverso = true;
		}
		else
		{
			stampaMargine( MARGINE_SINISTRO, " " ); 
			if ( gameSettings::nCaselleTab - casella >= num_elementi )
			{
				cout << "║ ";
				for ( int colonna = num_elementi - 1; colonna >= 0 && casella < gameSettings::nCaselleTab; colonna--, casella++ )
				{
					cout << " ";
					stampaCarattere( matrix[riga][colonna] );
					cout << " ";
				}
			}
			else
			{
				int rimanente = num_elementi - ( gameSettings::nCaselleTab - casella );
				stampaMargine( rimanente * 3, " " );
				cout << "║ ";
				for ( int colonna = ( gameSettings::nCaselleTab - casella ) % SEGMENTO- 1; colonna >= 0 && casella < gameSettings::nCaselleTab; colonna--, casella++ )
				{
					cout << " ";
					stampaCarattere( matrix[riga][colonna] );
					cout << " ";
				}
			}
			
			cout << " ║\n";

			if ( casella < gameSettings::nCaselleTab )
			{
				// INTERLINEA 1
				stampaMargine( MARGINE_SINISTRO, " " );
				cout << "║  ";
				stampaCarattere( matrix[riga][num_elementi] );
				cout << "  ╔";
				casella++;
				stampaMargine( ( num_elementi - 1 ) * 3 - 1, "═" );
				cout << "╝\n";
			}
			
			int rimanente = gameSettings::nCaselleTab - casella - 2;	//  Memorizzo il numero delle caselle rimanenti

			if ( casella < gameSettings::nCaselleTab )
			{
				// INTERLINEA 2
				if ( rimanente >= num_elementi )	//  Dopo l'interlinea c'è il segmento standard con 18 elementi
				{	//  |     ║  *  ╚════════════════════╗
					stampaMargine( MARGINE_SINISTRO, " " );
					cout << "║  ";
					stampaCarattere( matrix[riga][num_elementi + 1] );
					cout << "  ╚";
					casella++;
					stampaMargine( ( num_elementi - 1 ) * 3 - 1, "═" );
					cout << "╗\n";
				}
				else	//  Dopo l'interlinea ci sono meno di 18 elementi
				{
					if ( rimanente <= 0 )
					{	//  |     ║  *  ║
						stampaMargine( MARGINE_SINISTRO, " " );
						cout << "║  ";
						stampaCarattere( matrix[riga][num_elementi + 1] );
						cout << "  ║\n";
						casella++;
					}
					else
					{	//  |     ║  *  ╚══════════╗
						stampaMargine( MARGINE_SINISTRO, " " );
						cout << "║  ";
						stampaCarattere( matrix[riga][num_elementi + 1] );
						cout << "  ╚";
						casella++;
						stampaMargine( ( rimanente - 1 ) * 3 + 2, "═" );
						cout << "╗\n";
					}
				}
			}	
			inverso = false;
		}
	}
}

void stampaCarattere( char carattere )
{
	int colore_base = 31;
	string fine_formattazione = "\033[0m";

	string stringa_colorata = "\033[1;" + to_string( ( colore_base + ( (int)carattere - (int)'A' ) ) ) + "m";

	if ( carattere == '*' || carattere == '?' )
		cout << "\033[1;37m" << carattere << fine_formattazione;	//  Stampo in grassetto bianco
	else
		cout << stringa_colorata << carattere << fine_formattazione;	//  Stampo in grassetto del colore del giocatore
}

void stampaGioco( ptr_partecipanti ptr_lista, ptr_tabellone tabellone )
{
	ptr_tabellone percorso = tabellone;
	int h = - 1, centrale = 1, fondo = 0;
/*	
    //  MODELLO 58 CASELLE
	cout << "\n\n"
		<< "           ╔════════════════════════════════════════════════════════╗\n"     -+    
		<< "           ║  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  ║\n"      |
		<< "           ╚══════════════════════════════════════════════════╗  *  ║\n"      |  
		<< "           ╔══════════════════════════════════════════════════╝  *  ║\n"      |
		<< "           ║  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  ║\n"      |  asse_y
		<< "           ║  *  ╔══════════════════════════════════════════════════╝\n"      |
		<< "           ║  *  ╚══════════════════════════════════════════════════╗\n"      |
		<< "           ║  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  *  ║\n"      |
		<< "           ╚════════════════════════════════════════════════════════╝\n\n";  -+
*/

	int asse_y;
	( ( gameSettings::nCaselleTab ) % SEGMENTO ) > 0 ? asse_y = ( ( gameSettings::nCaselleTab ) / SEGMENTO ) + 1 : asse_y = ( gameSettings::nCaselleTab ) / SEGMENTO;

	char matrix[asse_y][20];
	aggiornaMatrice( matrix, ptr_lista, tabellone );

	gestioneTerminale::wait( 1 );
	stampaMatrice( matrix );

	cout << "\033[0m\n\n";	//  Cancellazione della formattazione

	//  Tabella riassuntiva giocatori
	cout << "  LUNGHEZZA TOTALE PERCORSO: " << gameSettings::nCaselleTab << "\n";
	ptr_partecipanti giocatori = ptr_lista;

	int colors = 31;
	char player_pointers = 'A';

	gestioneTerminale::wait( 1 );  //  Delay
	while ( giocatori != NULL )	//  Stampo la tabella dei giocatori e le loro posizioni
	{
		string str_colors = "\033[1;" + to_string( colors ) + "m";
		Giocatore tmp = giocatori->giocatore;
		cout << "  " << str_colors << "Giocatore " << player_pointers << "\033[0m [" << tmp.getNome() << "]: " << tmp.getPosizione() << "\n";
		colors++;
		player_pointers++;
		giocatori = giocatori->successivo;	
	}

	gestioneTerminale::wait( 1 );	//  Delay

	cout << "\n\n";
	cout << "  ╔═════════════════════════════════════════╗\n";
	cout << "  ║  LEGENDA       \033[1;37m*\033[0m   Casella normale      ║\n";
	cout << "  ║                \033[1;37m?\033[0m   Casella con effetto  ║\n";
	cout << "  ║            \033[1;31mA\033[0m - \033[1;36mF\033[0m   Giocatori            ║\n";
	cout << "  ╚═════════════════════════════════════════╝\n\n";

/*	cout << "  ╔══════════════════════════════════════════════════╗\n";
	cout << "  ║                     LEGENDA                      ║\n";
	cout << "  ║--------------------------------------------------║\n";
	cout << "  ║  + simboleggia la presenza di una o più pedine   ║\n";
	cout << "  ║--------------------------------------------------║\n";
	cout << "  ║  0 casella vuota                                 ║\n";
	cout << "  ║--------------------------------------------------║\n";
	cout << "  ║  1 effetto pesca una carta                       ║\n";
	cout << "  ║--------------------------------------------------║\n";
	cout << "  ║  2 effetto tira il dado e blocca il giocatore    ║\n";
	cout << "  ║--------------------------------------------------║\n";
	cout << "  ║  3 effetto avanza in modo casuale                ║\n";
	cout << "  ║--------------------------------------------------║\n";
	cout << "  ║  4 effetto indietreggia in modo casuale          ║\n";
	cout << "  ║--------------------------------------------------║\n";
	cout << "  ║  5 effetto ritira il dado e avanza               ║\n";
	cout << "  ║--------------------------------------------------║\n";
	cout << "  ║  6 effetto ritira il dado e indietreggia         ║\n";
	cout << "  ║--------------------------------------------------║\n";
	cout << "  ║  7 effetto tira il dado se il numero è pari      ║\n";
	cout << "  ║    avanza del numero se è dispari rimani         ║\n";
	cout << "  ║    bloccato il numero di volte uscito            ║\n";
	cout << "  ╚══════════════════════════════════════════════════╝\n\n";*/
}
