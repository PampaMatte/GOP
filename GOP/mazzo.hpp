/*
 * casella.cpp
 *
 *  Created on: 23 gen 2018
 *      Author: Simone Grillini, Matteo Pampaloni & Emanuele Querzola
 */
#ifndef MAZZO_H_
#define MAZZO_H_

#include <iostream>
#include <cstdlib>
#include <stdio.h>
#include <time.h>
#include "funzioniGioco.hpp"
using namespace std;

struct nodo{
	int val;
	nodo *next;
};
typedef nodo *ptr_lista;

class Mazzo{
	private:

		int posizione;
		ptr_lista head;

	public:

		ptr_lista crea_mazzo();	// crea l'intero mazzo sotto forma di lista dove ogni carta � un nodo
		Mazzo();
		void aggiorna_pos();	//tiene traccia della posizione del nodo(carta) da utilizzare incrementato ad ogni carta utilizzata in modo da scorrere il mazzo
		int getPosizione();	//ritorna la posizione corrente nella lista
		void setPosizione(int n);
		ptr_lista getHead();		//restituisce head
		int pesca_carta();
		void effetto_usocarta(ptr_partecipanti ptr_lista, int nGiocatore);
		void stampa_mazzo();
};

#endif /* MAZZO_H_ */
