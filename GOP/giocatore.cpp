/*
 * casella.cpp
 *
 *  Created on: 23 gen 2018
 *      Author: Simone Grillini, Matteo Pampaloni & Emanuele Querzola
 */


#include <string>
#include "giocatore.hpp"
#include "funzioniGioco.hpp"

using namespace std;

Giocatore::Giocatore (string nome){

	setNome(nome);
	//setta i parametri a 0 per cominciare la partita
	setPosizione(0,true);
	setBloccato(0,true);

}

void Giocatore::setNome (string nome){

	this->nome=nome;

}

void Giocatore::setPosizione (int n, bool cambia){
	if (cambia)
		posizione=n;
	else{
		if (posizione+n <= gameSettings::nCaselleTab && posizione+n >= 0)
			posizione += n;
		else if (posizione+n >= 0)
			posizione = gameSettings::nCaselleTab + (gameSettings::nCaselleTab - posizione - n);
		else 
			posizione = -posizione - n;
	}
}

void Giocatore::setBloccato (int n, bool cambia){

	if (cambia) //controllo se devo cambiare il valore, altrimenti incremento
			bloccato=n;
		else
			bloccato+=n;

}

string Giocatore::getNome (){

	return nome;

}

int Giocatore::getPosizione(){

	return posizione;

}

int Giocatore::getBloccato(){

	return bloccato;

}
